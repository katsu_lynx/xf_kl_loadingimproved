/**
 * KL_LoadingImproved_LoadingImproved
 *
 *	@author: Katsulynx
 *  @last_edit:	05.10.2015
 *  @compiled: 05.10.2015 - Closure Compiler
 */
!function(b,f,c,g){XenForo.AjaxProgress=function(){var a=null,d=function(){b(".Progress, .xenForm .ctrlUnit.submitUnit dt").addClass("InProgress");a||(a=b("<div/>",{id:"AjaxProgress"}).append(overlayStructure).appendTo("body").overlay({top:0,speed:XenForo.speed.fast,oneInstance:!1,closeOnClick:!1,closeOnEsc:!1}).data("overlay"));a.load()},e=function(){b(".Progress, .xenForm .ctrlUnit.submitUnit dt").removeClass("InProgress");a&&a.isOpened()&&a.close()};b(c).bind({ajaxStart:function(a){XenForo._AjaxProgress=
!0;d()},ajaxStop:function(a){XenForo._AjaxProgress=!1;e()},PseudoAjaxStart:function(a){d()},PseudoAjaxStop:function(a){e()}});b.browser.msie&&7>b.browser.version&&b(c).bind("scroll",function(c){a&&a.isOpened()&&!a.getConf().fixed&&a.getOverlay().css("top",a.getConf().top+b(f).scrollTop())})}}(jQuery,this,document);