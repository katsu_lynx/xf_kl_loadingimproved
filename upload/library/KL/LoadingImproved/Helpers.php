<?php

/**
 * KL_LoadingImproved_Helpers
 *
 * @author: Nerian
 * @last_edit:    05.10.2015
 */
class KL_LoadingImproved_Helpers
{
    public static function helperJSEncodeString($string = null)
    {
        return json_encode($string);
    }
}