<?php

/**
 * KL_LoadingImproved_Listener_Helper
 *
 * @author: Nerian
 * @last_edit:    05.10.2015
 */
class KL_LoadingImproved_Listener_Helper
{
    public static function extend(XenForo_Dependencies_Abstract $dependencies, array $data)
    {
        //Get the static variable $helperCallbacks and add a new item in the array.
        XenForo_Template_Helper_Core::$helperCallbacks += array(
            'jsencodestring' => array('KL_LoadingImproved_Helpers', 'helperJSEncodeString'),
        );
    }
}